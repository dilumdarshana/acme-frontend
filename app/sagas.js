import { fork, all } from 'redux-saga/effects';
import IceCreamListSagas from './containers/ice_cream_list/sagas';

export default function* rootSaga() {
    return yield all([
        fork(IceCreamListSagas),
    ]);
}
