import {
    LIST_ICE_CREAM,
    LIST_ICE_CREAM_SUCCESS,
    LIST_ICE_CREAM_FAILED,
} from '../../constants/action_types/iceCreamList';

const getIceCreamList = data => ({
    type: LIST_ICE_CREAM,
    data,
});

const getIceCreamListSuccess = response => ({
    type: LIST_ICE_CREAM_SUCCESS,
    response,
});

const getIceCreamListFailed = error => ({
    type: LIST_ICE_CREAM_FAILED,
    error,
});

export {
    getIceCreamList,
    getIceCreamListSuccess,
    getIceCreamListFailed,
};
