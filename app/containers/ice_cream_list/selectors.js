import { createSelector } from 'reselect';

const selectIceCreamListState = state => state.iceCreamList;

const selectGetIceCreamList = () => createSelector(
    selectIceCreamListState,
    currentState => currentState.iceCreamList,
);

const selectGetIceCreamListSuccess = () => createSelector(
    selectIceCreamListState,
    currentState => currentState.iceCreamListSuccess,
);

const selectGetIceCreamListFailed = () => createSelector(
    selectIceCreamListState,
    currentState => currentState.iceCreamListError,
);

export {
    selectGetIceCreamList,
    selectGetIceCreamListSuccess,
    selectGetIceCreamListFailed,
};
