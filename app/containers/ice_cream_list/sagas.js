import { takeEvery, put, call } from 'redux-saga/effects';
import {
    LIST_ICE_CREAM,
    LIST_ICE_CREAM_SUCCESS,
    LIST_ICE_CREAM_FAILED,
} from '../../constants/action_types/iceCreamList';
import {
    getIceCreamListSuccess,
    getIceCreamListFailed,
} from './actions';
import httpRequests from '../../helpers/httpRequests';

export function* getIceCreamList() {
    try {
        const result = yield call(httpRequests.iceCreamsList);

        if (result.data.status) {
            yield put(getIceCreamListSuccess(result.data));
        } else {
            yield put(getIceCreamListFailed(result.data.message));
        }
    } catch (error) {
        yield put(getIceCreamListFailed(error.message));
    }
}

export default function* userSignUpSagas() {
    yield* [
        takeEvery(LIST_ICE_CREAM, getIceCreamList),
    ];
}
