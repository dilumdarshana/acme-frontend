import React, { useEffect, useCallback } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { 
    getIceCreamList,
 } from './actions';
 import {
    selectGetIceCreamList,
    selectGetIceCreamListSuccess,
    selectGetIceCreamListFailed,
 } from './selectors';

const IceCreamList = ({ getIceCreamListFunc, iceList, iceListSuccess }) => {
    useEffect(() => {
        getIceCreamListFunc();
    }, []);

    let products = null;

    const doSelectFlavors = useCallback(() => {
        // TODO: open 
    }, []);

    if (iceListSuccess) {
        products = iceList.map((item, index) => {
            const { name, image, price } = item;
    
            return (
                <div key={`prod-${index}`} className="col-md-3 col-sm-6">
                    <div className="product-grid">
                        <div className="product-image">
                            <a href="#4">
                                <img className="pic" src={image} />
                            </a>
                        </div>
                        <div className="product-content">
                            <h3 className="title">
                                <a href="#4">{name}</a>
                            </h3>
                            <div className="price">${price.toFixed(2)}</div>
                            <button className="add-to-cart" onClick={doSelectFlavors}>+ Select</button>
                        </div>
                    </div>
                </div>
            )
        });
    }

    return (
        <div className="ice-cream-list">
            <h3 className="h3">Select your favorite</h3>
            <div className="row">
                {products}
            </div>
        </div>
    )
};

IceCreamList.propTypes = {
    getIceCreamListFunc: PropTypes.func.isRequired,
    iceList: PropTypes.array,
};

IceCreamList.defaultProps = {
    iceList: [],
};

const mapDispatchToProps = dispatch => ({
    getIceCreamListFunc: () => dispatch(getIceCreamList()),
});

const mapStateToProps = createStructuredSelector({
    iceList: selectGetIceCreamList(),
    iceListSuccess: selectGetIceCreamListSuccess(),
    iceListFailed: selectGetIceCreamListFailed(),
});

export default connect(mapStateToProps, mapDispatchToProps)(IceCreamList);
