import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import Loadable from 'react-loadable';
import Layout from './layout';
import { history } from './store';

import LoadingComponent from './components/common/loading';

const IceCreamList = Loadable({
    loader: () => import('./containers/ice_cream_list/list'),
    loading: LoadingComponent,
});

const Routes = () => (
    <ConnectedRouter history={history}>
        <Layout>
            <Switch>
                <Route exact path="/" component={IceCreamList} />
            </Switch>
        </Layout>
    </ConnectedRouter>
);

export default Routes;
