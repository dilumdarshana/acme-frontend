export const LIST_ICE_CREAM = 'LIST_ICE_CREAM';
export const LIST_ICE_CREAM_SUCCESS = 'LIST_ICE_CREAM_SUCCESS';
export const LIST_ICE_CREAM_FAILED = 'LIST_ICE_CREAM_FAILED';

export default {
    LIST_ICE_CREAM,
    LIST_ICE_CREAM_SUCCESS,
    LIST_ICE_CREAM_FAILED,
}
