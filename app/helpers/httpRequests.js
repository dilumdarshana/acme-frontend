import axios from 'axios';
import appConfig from 'appConfig';

const lstApiUrl = `${appConfig.api_base_url}`;

const httpRequests = {
    iceCreamsList: () => axios.get(`${lstApiUrl}/list_base`, null),
};

export default httpRequests;
