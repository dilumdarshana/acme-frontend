import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import iceCreamList from './ice_cream_list';

export default history => combineReducers({
    router: connectRouter(history),
    iceCreamList,
});
