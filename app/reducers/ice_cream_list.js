import {
    LIST_ICE_CREAM,
    LIST_ICE_CREAM_SUCCESS,
    LIST_ICE_CREAM_FAILED,
} from '../constants/action_types/iceCreamList';

const initialState = {
    iceCreamList: [],
    iceCreamListSuccess: false,
    iceCreamListError: null,
};

export default function iceCreamList(state = initialState, action) {
    switch (action.type) {
        case LIST_ICE_CREAM:
            return { ...state, iceCreamList: [], iceCreamListSuccess: false, iceCreamListError: null };
        case LIST_ICE_CREAM_SUCCESS:
            return { ...state, iceCreamList: action.response.data, iceCreamListSuccess: true, iceCreamListError: null };
        case LIST_ICE_CREAM_FAILED:
            return { ...state, iceCreamList: [], iceCreamListSuccess: true, updateCompanySuccess: false, iceCreamListError: action.response };
        default:
            return state;
    }
}
