import React from 'react';

const Header = () => (
    <header>
        <h2>Acme Ice Cream</h2>
    </header>
);

export default Header;
